/**
Programmer: Chris Pikula
Project: Ball Control
Date: 2016-11-15


Description: This is part of a program that turns on and off
LEDs via communicating to and from the console.
It can also run some timing tests, as well as use an interrupt timer
	(Via timers 2 and 3, respectively)
 
It can also setup the ball control system. 
 
This file is the user interface
*/
#include "userInterface.h"
#include "time_ops.h"
#include "timer_impl.h"
#include "ctype.h"

//we only want to initialize the timer3 for the ball control once.
static bool GLOBAL_IS_POSITIONING_ON = 0;


static const char * MsgTimeAndDate = __TIME__ " " __DATE__ " \n\r\0";
static const char * CONSOLEMsg = "Console>\0";

static const char * commandHELP = "HELP";
static const char cmdHELP = 'H';
static const char * commandLED = "LED ON ";
static const char cmdLED = 'O';
static const char * commandLEDOFF = "LED OFF ";
static const char cmdLEDOFF = 'F';
static const char * commandStatusLED = "STATUS";
static const char cmdStatusLED = 'S';
static const char * commandRunTest = "RUN_TEST";
static const char cmdRunTest = 'T';
static const char * commandDebug = "DEBUG";
static const char cmdDebug = 'D';
static const char * commandTimer = "TIMER";
static const char cmdTimer = 'I';
static const char * commandTimerOff = "TIMER OFF";
static const char cmdTimerOff = 'J';
static const char * commandPWM = "PWM";
static const char cmdPWM = 'P';
static const char * commandAngle = "ANGLE";
static const char cmdAngle = 'A';
static const char * commandPWMOff = "PWM OFF";
static const char cmdPWMOff = 'Q';
static const char * commandPWMOn = "PWM ON";
static const char cmdPWMOn = 'B';
static const char * commandHeight = "HEIGHT";
static const char cmdHeight = 'h';
static const char * commandGOTO = "GOTO";
static const char cmdGOTO = 'G';
//Error
static const char cmdError = 'E';
static const char cmdErrorLength = 'R';

static const char * ERRORMsg = "Error!\n\r\0";
static const char * ERRORMsg1 = "Invalid Input!\n\r\0";
static const char * ERRORMsg2 =	"Type HELP for a list of correct commands\n\r\0";
static const char * RETURNMsg = "\n\r\0";
static const char * ERRORMsgCommandTooLong = "Command Too Long!\n\r\0";
static const char * ERRORMsgWrongCommandLength = "Command Wrong Length!\n\r\0";
static const char * HELPMsg1 = "To use this program\n\r\0";
static const char * HELPMsg2 = "Type HELP to get this message\n\r\0";
static const char * HELPMsg3 = "Type LED ON x, where x is from 0 to 7\n\r\0";
static const char * HELPMsg4 = "That will turn on a specified LED\n\r\0";
static const char * HELPMsg5 = "Type LED OFF x, where x is from 0 to 7\n\r\0";
static const char * HELPMsg6 = "That will turn off a specified LED\n\r\0";	
static const char * HELPMsg7 = "Typing STATUS will return the status of the LEDs\n\r\0";
static const char * HELPMsg8 = "Typing RUN_TEST will run a series of timing tests\n\r\0";
static const char * HELPMsg9 = "Typing TIMER will run an interrupt timer and blink LED 0\n\r\0";
static const char * HELPMsg10 = "Typing TIMER OFF will turn off the interrupt timer\n\r\0";
static const char * HELPMsg11 = "Typing ANGLE will let you change the orientation of the \n\r\0";
static const char * HELPMsg12 = "servo to that many tenths of degrees.\n\r\0";
static const char * HELPMsg13 = "Typing PWM will let you set the angle of the motor in percent\n\r\0";
static const char * HELPMsg14 = "Typing PWM ON will initialize the motor\n\r\0";
static const char * HELPMsg15 = "Typing PWM OFF will stop updating the motor\n\r\0";
static const char * HELPMsg16 = "Typing HEIGHT will return the current height of the ball.\n\r\0";
static const char * HELPMsg17 = "Typing GOTO will let you go to a specific height.\n\r\0";
static const char * LEDMsgOn = "You turned on LED \0";
static const char * LEDMsgOff = "You turned off LED \0";
static const char * LEDStatusMsg = "The LEDs that are on are: \n\r\0";
static const char * HeightMsg1 = "The current height of the ball is \0";
static const char * HeightMsg2 = " cm.\n\r\0";
static const char * TIMERStatusMsgOn = "The timer was successfully turned on. \n\r\0";
static const char * TIMERStatusMsgOff = "The timer was successfully turned off. \n\r\0";
static const char * NUMBERMsg = "Please enter a number beween ";
static const char * GOTOMsg1 = "Send the Ball to what height?\n\r\0";
void UIConsole(void)
{
	USART2SendString(CONSOLEMsg);
}

void UIMessageTooLong(void)
{	
	USART2SendString(ERRORMsg);
	USART2SendString(RETURNMsg);	
	USART2SendString(ERRORMsgCommandTooLong);
}

bool CompareCharArrays(const char * typedInput, const int8_t length, const char * comparedInput)
{
	for(int8_t i = 0; i < length-1; i++)
	{
		if(typedInput[i] != comparedInput[i])
		{
			return false;
		}
	}
	return true;
}

char UIRemoveBackspace(uint8_t &stringBuffLocation)
{
	uint8_t returnedValue;
	stringBuffLocation--;
	if(stringBuffLocation >0)
	{
		stringBuffLocation--;
	}
	returnedValue = USART2SendByte(' ');
	returnedValue = USART2SendByte('\b');
	
	if(stringBuffLocation == 0)
	{
		return 0;
	}
	else
	{
		return returnedValue;	
	}
}


uint16_t UILEDCommand(char command)
{
	int32_t numericInput = 0;
	if (command == cmdHELP)
	{
		
		//USART2SendString( );
		USART2SendString(MsgTimeAndDate);
		USART2SendString(HELPMsg1);
		USART2SendString(HELPMsg2);
		USART2SendString(HELPMsg3);
		USART2SendString(HELPMsg4);
		USART2SendString(HELPMsg5);
		USART2SendString(HELPMsg6);
		USART2SendString(HELPMsg7);
		USART2SendString(HELPMsg8);
		USART2SendString(HELPMsg9);
		USART2SendString(HELPMsg10);
		USART2SendString(HELPMsg11);
		USART2SendString(HELPMsg12);
		USART2SendString(HELPMsg13);
		USART2SendString(HELPMsg14);
		USART2SendString(HELPMsg15);
		USART2SendString(HELPMsg16);
		USART2SendString(HELPMsg17);
	}
	else if (command == cmdError)
	{
		USART2SendString(ERRORMsg);
		USART2SendString(ERRORMsg1);
		USART2SendString(ERRORMsg2);
	}
	else if (command == cmdErrorLength)
	{
		USART2SendString(ERRORMsg);
		USART2SendString(ERRORMsgWrongCommandLength);
		USART2SendString(ERRORMsg2);
	}
	else if (command == cmdRunTest)
	{
		USART2SendString("Running Timing Tests:\n\r\0");
		
		TimerInit();
		time_ops();
		timer_shutdown();
	}
	//turn on a bit
	else if (command < 8)
	{
		turnOnABitBSRR(&GPIOB_BSRR, command + 8);	
		USART2SendString(LEDMsgOn);
		USART2SendNumber(command);
		USART2SendString(RETURNMsg);	
	}
	//turn off a bit
	else if (command >= 8 && command < 16)
	{
		turnOffABitBSRR(&GPIOB_BSRR, command);
		USART2SendString(LEDMsgOff);
		USART2SendNumber(command-8);
		USART2SendString(RETURNMsg);	
	}
	else if (command == cmdStatusLED)
	{
		USART2SendString(LEDStatusMsg);
	}
	else if (command == cmdDebug)
	{
		//timer_init_with_interrupts();
	}
	else if (command == cmdTimer)
	{
		timer_init_with_interrupts();
		USART2SendString(TIMERStatusMsgOn);
	}
	else if (command == cmdTimerOff)
	{
		timer3_shutdown();
		USART2SendString(TIMERStatusMsgOff);
	}
	else if (command == cmdPWM)
	{
		//Query for PWM number between x and y

		do
		{
		numericInput = UIInputNumber(0, 100);
		}while(numericInput > 100);
		//initialize PWM with value
		numericInput = (numericInput * 16 + 600)*2;
		
		TIM4->CCR2 = uint16_t(numericInput);
		
	}
	else if (command == cmdAngle)
	{
		//Query for Angle number between x and y
		do
		{
			numericInput = UIInputNumber(0,1600);
		}while(numericInput > 1600);
		
		numericInput = (numericInput + 600)*2;
		//40000 = 20000
		TIM4->CCR2 = uint16_t(numericInput);
		
		//calculate angle
		//initialize PWM with factored angle
	}
	else if (command == cmdPWMOff)
	{
			timer4shutdown();
	}
	else if (command == cmdPWMOn)
	{
		timer4Init();
	}
	else if (command == cmdHeight)
	{
		FindPositionSensor();
		int height = FindPosition();
		//HEIGHT SETUP
		
		USART2SendString(HeightMsg1);
		USART2SendNumber32(height);
		USART2SendString(HeightMsg2);
	}
	else if (command == cmdGOTO)
	{
		USART2SendString(GOTOMsg1);
		do
		{
			numericInput = UIInputNumber(20, 40);//(10,60);
		}while(numericInput > 40 || numericInput < 20);
		
		///////////////////////LINEARINTERPOLATIONMARK2
		//map 20 2000, map 40 to 1000
		numericInput = numericInput * -50 + 3000;
		
		//do math to convert numericInput;
		if (GLOBAL_IS_POSITIONING_ON == 0)
		{
			GLOBAL_IS_POSITIONING_ON = 1;
			Timer2INIT();
		}
		return numericInput;
		
	}
	return 0;
}

uint32_t UIInputNumber(uint32_t min, uint32_t max)
{
	char stringBuff[10] = {'\0'};
	stringBuff[9] = '\0';
	uint8_t stringBuffLocation = 0;
	int8_t returnedValue = ' ';
	int32_t numericValue = 0;
	USART2SendString(NUMBERMsg);
	USART2SendNumber32(min);
	USART2SendString(" and ");
	USART2SendNumber32(max);
	USART2SendString(":\n\r\0");
	UIConsole();
	while(1)
	{
		returnedValue = USART2GetByte(' ');
		USART2SendByte(returnedValue);
		stringBuff[stringBuffLocation] = returnedValue;
		stringBuffLocation++;
		if(returnedValue == '\b' || returnedValue == 127 || stringBuffLocation > 7) 
		{
			UIRemoveBackspace(stringBuffLocation);
		}
		if(returnedValue == '\r')
		{
			USART2SendByte('\n');
			if(stringBuffLocation > 6)
			{
				UIMessageTooLong();
			}
			else
			{
				//USART2SendString("In MatchString\n");
				numericValue = UIAtoI(stringBuff);////////////////////////////////
				
				break;
			}
			stringBuffLocation = 0;
			stringBuff[0] = '\0';
			stringBuff[1] = '\0';
			stringBuff[2] = '\0';
			stringBuff[3] = '\0';
			stringBuff[4] = '\0';
			stringBuff[5] = '\0';
			stringBuff[6] = '\0';
			stringBuff[7] = '\0';
			stringBuff[8] = '\0';
			stringBuff[9] = '\0';
			USART2SendString(NUMBERMsg);
			USART2SendNumber32(min);
			USART2SendString(" and ");
			USART2SendNumber32(max);
			USART2SendString(":\n\r\0");
			UIConsole();
		}
	}
	return numericValue;
}

uint32_t UIAtoI(char* input)
{
	uint32_t calculatedValue = 0;
	for (int i = 0; input[i] != '\r'; i++)
	{
		calculatedValue = calculatedValue * 10 + (input[i]-48);
	}
	//USART2SendNumber32(calculatedValue);
	return calculatedValue;
}

char UImatchString(char* input, int8_t length)
{
	int status=-1;//-1 for start, 0 for mid, +1 for trailing
	int start = 0;
	int end = 0;
	int i = 0;
	char command = '0';
	char LEDVar = '0';
	//std::string revisedInput = "";
	//std::stringstream ss;
	USART2SendByte('\n');
	USART2SendByte('\r');
	
	//Trim leading & trailing whitespace
	for (i = 0; i < length; i++)
	{
		if (status == -1 && input[i] == ' ') //space
		{
			start++;
		}	
		else if (status == 0 && input[i] == ' ') //space
		{
			end++;
		}
		else if (input[i] != ' ' && input[i] != '\r')
		{
			status = 0;
			end = 0;
		}
	}
	//subtract away leading and trailing whitespace
	for (i = 0; i < (length - start - end); i++)
	{
		input[i] = input[i+start];
	}
	length = length - start - end;
	input[length] = '\r';

	//Set all lowercase char's to uppercase
	for (i = 0; i< length; i++)
	{
		if (input[i] > 96 && input[i] < 123)
		{
			input[i] = input[i] - 32;
		}
	}
	

	if (length == 5)
	{
		if (CompareCharArrays(input, length, commandHELP))
		{
			return cmdHELP;
		}
		else if (CompareCharArrays(input, length, commandGOTO))
		{
			return cmdGOTO;
		}
		else
		{
			return cmdError;
		}
	}
	else if (length == 4)
	{
		if (CompareCharArrays(input, length, commandPWM))
		{
			return cmdPWM;
		}
		else
		{
			return cmdError;
		}
	}
	else if (length == 6)
	{
		if (CompareCharArrays(input, length, commandDebug))
		{
			return cmdDebug;
		}
		else if (CompareCharArrays(input, length, commandTimer))
		{
			return cmdTimer;
		}
		else if (CompareCharArrays(input, length, commandAngle))
		{
			return cmdAngle;
		}
		else
		{
			return cmdError;
		}
	}
	else if (length == 7)
	{
		if (CompareCharArrays(input, length, commandStatusLED))
		{
			return cmdStatusLED;
		}
		else if (CompareCharArrays(input, length, commandPWMOn))
		{
			return 'B';
		}
		else if (CompareCharArrays(input, length, commandHeight))
		{
			return cmdHeight;
		}
		else
		{
			return cmdError;
		}
	}
	else if (length == 8)
	{
		if (CompareCharArrays(input, length, commandPWMOff))
		{
			return cmdPWMOff;
		}
		else
		{
			return cmdError;
		}
	}
	else if (length == 9)
	{
		if (CompareCharArrays(input, length -1, commandLED))
		{
			command = cmdLED;
		}
		else if (CompareCharArrays(input, length, commandRunTest))
		{
			return cmdRunTest;
		}
		else
		{
			return cmdError;
		}
	}
	else if (length == 10)
	{
		if (CompareCharArrays(input, length -1, commandLEDOFF))
		{
			command = cmdLEDOFF;
		}
		else if (CompareCharArrays(input, length, commandTimerOff))
		{
			return cmdTimerOff;
		}
		else
		{
			return cmdError;
		}
	}
	else
	{
		return cmdErrorLength;
	}
	if (command == cmdLED || command == cmdLEDOFF)
	{
		LEDVar = input[command==cmdLED?7:8];

			if (LEDVar == '0')
			{
				if (command == cmdLED)
				{
					command = 0;
				}
				else
				{
					command = 8;
				}
			}
			else if (LEDVar == '1')
			{
				if (command == cmdLED)
				{
					command = 1;
				}
				else
				{
					command = 9;
				}
			}
			else if (LEDVar == '2')
			{
				if (command == cmdLED)
				{
					command = 2;
				}
				else
				{
					command = 10;
				}
			}
			else if (LEDVar == '3')
			{
				if (command == cmdLED)
				{
					command = 3;
				}
				else
				{
					command = 11;
				}
			}
			else if (LEDVar == '4')
			{
				if (command == cmdLED)
				{
					command = 4;
				}
				else
				{
					command = 12;
				}
			}
			else if (LEDVar == '5')
			{
				if (command == cmdLED)
				{
					command = 5;
				}
				else
				{
					command = 13;
				}
			}
			else if (LEDVar == '6')
			{
				if (command == cmdLED)
				{
					command = 6;
				}
				else
				{
					command = 14;
				}
			}
			else if (LEDVar == '7')
			{
				if (command == cmdLED)
				{
					command = 7;
				}
				else
				{
					command = 15;
				}
			}
			else
			{
				command = cmdError;
			}
	}
	return command;
}
