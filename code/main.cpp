/*
    FreeRTOS V7.2.0 - Copyright (C) 2012 Real Time Engineers Ltd.


    ***************************************************************************
     *                                                                       *
     *    FreeRTOS tutorial books are available in pdf and paperback.        *
     *    Complete, revised, and edited pdf reference manuals are also       *
     *    available.                                                         *
     *                                                                       *
     *    Purchasing FreeRTOS documentation will not only help you, by       *
     *    ensuring you get running as quickly as possible and with an        *
     *    in-depth knowledge of how to use FreeRTOS, it will also help       *
     *    the FreeRTOS project to continue with its mission of providing     *
     *    professional grade, cross platform, de facto standard solutions    *
     *    for microcontrollers - completely free of charge!                  *
     *                                                                       *
     *    >>> See http://www.FreeRTOS.org/Documentation for details. <<<     *
     *                                                                       *
     *    Thank you for using FreeRTOS, and thank you for your support!      *
     *                                                                       *
    ***************************************************************************


    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
    >>>NOTE<<< The modification to the GPL is included to allow you to
    distribute a combined work that includes FreeRTOS without being obliged to
    provide the source code for proprietary components outside of the FreeRTOS
    kernel.  FreeRTOS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details. You should have received a copy of the GNU General Public
    License and the FreeRTOS license exception along with FreeRTOS; if not it
    can be viewed here: http://www.freertos.org/a00114.html and also obtained
    by writing to Richard Barry, contact details for whom are available on the
    FreeRTOS WEB site.

    1 tab == 4 spaces!
    
    ***************************************************************************
     *                                                                       *
     *    Having a problem?  Start by reading the FAQ "My application does   *
     *    not run, what could be wrong?                                      *
     *                                                                       *
     *    http://www.FreeRTOS.org/FAQHelp.html                               *
     *                                                                       *
    ***************************************************************************

    
    http://www.FreeRTOS.org - Documentation, training, latest information, 
    license and contact details.
    
    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool.

    Real Time Engineers ltd license FreeRTOS to High Integrity Systems, who sell 
    the code with commercial support, indemnification, and middleware, under 
    the OpenRTOS brand: http://www.OpenRTOS.com.  High Integrity Systems also
    provide a safety engineered and independently SIL3 certified version under 
    the SafeRTOS brand: http://www.SafeRTOS.com.
*/

/*
 * This is a very simple demo that demonstrates task and queue usages only in
 * a simple and minimal FreeRTOS configuration.  Details of other FreeRTOS 
 * features (API functions, tracing features, diagnostic hook functions, memory
 * management, etc.) can be found on the FreeRTOS web site 
 * (http://www.FreeRTOS.org) and in the FreeRTOS book.
 *
 * Details of this demo (what it does, how it should behave, etc.) can be found
 * in the accompanying PDF application note.
 *
*/

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

#include "utilities.h"
//Used for turnOnABit, turnOffABit, and delay
#include "userInterface.h"

#include "timer_impl.h"
#include "time_ops.h"

/* Standard include. */
#include <stdio.h>

/* Priorities at which the tasks are created. */
#define mainQUEUE_RECEIVE_TASK_PRIORITY     ( tskIDLE_PRIORITY + 2 )
#define mainQUEUE_SEND_TASK_PRIORITY        ( tskIDLE_PRIORITY + 1 )
#define mainTASK1_PRIORITY                  ( tskIDLE_PRIORITY + 3 )
#define mainTASK0_PRIORITY                  ( tskIDLE_PRIORITY + 3 )
#define mainTASK_CONSOLE_PRIORITY						( tskIDLE_PRIORITY + 3 )

/* The rate at which data is sent to the queue, specified in milliseconds. */
#define mainQUEUE_SEND_FREQUENCY_MS         ( 10 / portTICK_RATE_MS )

/* The number of items the queue can hold.  This is 1 as the receive task
will remove items as they are added, meaning the send task should always find
the queue empty. */
#define mainQUEUE_LENGTH                    ( 1 )

/* The ITM port is used to direct the printf() output to the serial window in 
the Keil simulator IDE. */
#define mainITM_Port8(n)    (*((volatile unsigned char *)(0xE0000000+4*n)))
#define mainITM_Port32(n)   (*((volatile unsigned long *)(0xE0000000+4*n)))
#define mainDEMCR           (*((volatile unsigned long *)(0xE000EDFC)))
#define mainTRCENA          0x01000000

/*-----------------------------------------------------------*/

/*
 * The tasks as described in the accompanying PDF application note.
 */
static void prvQueueReceiveTask( void *pvParameters );
static void prvQueueSendTask( void *pvParameters );
static void task0( void *pvParameters );
static void task1( void *pvParameters );

static void console( void *pvParameters );

/*
 * Redirects the printf() output to the serial window in the Keil simulator
 * IDE.
 */
extern "C" 
{
int fputc( int iChar, FILE *pxNotUsed );
}
/*-----------------------------------------------------------*/

/* The queue used by both tasks. */
static xQueueHandle xQueue = NULL;

/***********************************************
My Code
*/
//gives us our ADC conversion result.
extern int ADC_CONVERSION_RESULT;

//Lets us know if our ADC conversion result is finished.
static bool GLOBAL_ADC_CONVERSION_FINISHED = 0;

//The current height of the ball, in terms of A2D conversion results.
static int32_t GLOBAL_BALLHEIGHT = 0;

//The current amount we are changing the ballheight by
static int16_t GLOBAL_BALLHEIGHT_DELTA = 0;

//The current value we are setting the PWM to.
static int32_t GLOBALBallPWM = 1125;

extern "C" 
{
	//Timer 3 can blink an LED
	void TIM3_IRQHandler(void)
	{
		TIM3->SR &= ~TIM_SR_UIF;
		GPIOB->ODR ^= GPIO_ODR_ODR15;	
	}
	
	//Timer2 can control the ball height
	void TIM2_IRQHandler(void)
	{
	uint16_t BallPWMUpperLimit = 750;
	int16_t BallPWMDeltaUL = 15;
	uint16_t BallPWMLowerLimit = 550;
	int16_t BallPWMDeltaLL = -15;	

		TIM2->SR &= ~TIM_SR_UIF;
		ADC1->CR2 |= ADC_CR2_SWSTART;
		//GPIOB->ODR ^= GPIO_ODR_ODR15;	
		
				if (GLOBAL_BALLHEIGHT != 0 && GLOBAL_ADC_CONVERSION_FINISHED == 1)
					GLOBAL_ADC_CONVERSION_FINISHED = 0;
			{
				
				//Set it only once
				
				//is the ballheight result below the adc result?
				//aka, are we going up?
				if (GLOBAL_BALLHEIGHT < ADC_CONVERSION_RESULT)
				{
					GLOBAL_BALLHEIGHT_DELTA += 5;
				}
				else
					//we are going down
				{
					GLOBAL_BALLHEIGHT_DELTA -= 5;
				}
				
				if(GLOBAL_BALLHEIGHT_DELTA < BallPWMDeltaLL)
				{
					GLOBAL_BALLHEIGHT_DELTA = BallPWMDeltaLL;
				}
				else if(GLOBAL_BALLHEIGHT_DELTA > BallPWMDeltaUL)
				{
					GLOBAL_BALLHEIGHT_DELTA = BallPWMDeltaUL;
				}
				
				GLOBALBallPWM += GLOBAL_BALLHEIGHT_DELTA;
				
				if(GLOBALBallPWM < BallPWMLowerLimit)
				{
					GLOBALBallPWM = BallPWMLowerLimit;	
				}
				else if (GLOBALBallPWM > BallPWMUpperLimit)
				{
					GLOBALBallPWM = BallPWMUpperLimit;
				}
				
				TIM4->CCR2 = uint16_t((GLOBALBallPWM + 600)*2);
				
			}
	}
	//ADC1 channel 2 can get the ADC converision result, and store it.
	void ADC1_2_IRQHandler(void)
	{
		ADC_CONVERSION_RESULT = ADC1->DR;
//		USART2SendNumber32(ADC_CONVERSION_RESULT);
//		USART2SendString("\n\r\0");

		ADC1->SR &= 0xFFFFFFE0;
		GLOBAL_ADC_CONVERSION_FINISHED = 1;
	}
}










/* One array position is used for each task created by this demo.  The 
variables in this array are set and cleared by the trace macros within
FreeRTOS, and displayed on the logic analyzer window within the Keil IDE -
the result of which being that the logic analyzer shows which task is
running when. */
unsigned long ulTaskNumber[ configEXPECTED_NO_RUNNING_TASKS ];

/*-----------------------------------------------------------*/
#include "main.h"

int main(void)
{
	portBInit();//Set all the pins of port B to push-pull, 50MHz
	
	USART2Open();
  NVIC_EnableIRQ(TIM3_IRQn); 
	NVIC_EnableIRQ(TIM2_IRQn);
	NVIC_EnableIRQ(ADC1_2_IRQn);//ADC1_2_IRQHandler
	UIConsole();
	IRSensorInit();
	
		// Copied from blinkOneLight project
    // ----------------------------------------------------------------
    //setupRegs();

    //Need to setup the the LED's on the board
    
	
////		//* regRCC_APB2ENR |= 0x08; // Enable Port B clock
////    //* regGPIOB_ODR  &= ~0x0000FF00;          /* switch off LEDs                    */
////    //* regGPIOB_CRH  = 0x33333333; //50MHz  General Purpose output push-pull

    //Writing to this register will turn a light on. Bits 8-15 control the bank of lights
    
////	//* regGPIOB_BSRR = 1u<<8;  //Set bit 8
////    //* regGPIOB_BSRR = 1u<<11; // set bit 11
    //----------------------------------------------------------------

    /* Create the queue. */
    xQueue = xQueueCreate( mainQUEUE_LENGTH, sizeof( unsigned long ) );

    if( xQueue != NULL )
    {
        /* Start the two tasks as described in the accompanying application
        note. */
//         xTaskCreate( prvQueueReceiveTask, ( signed char * ) "Rx",
//                  configMINIMAL_STACK_SIZE, NULL,
//                  mainQUEUE_RECEIVE_TASK_PRIORITY, NULL );
//         xTaskCreate( prvQueueSendTask, ( signed char * ) "TX",
//                  configMINIMAL_STACK_SIZE, NULL,
//                  mainQUEUE_SEND_TASK_PRIORITY, NULL );

        xTaskCreate( task0, (signed char *) "TASK0",
                 configMINIMAL_STACK_SIZE, NULL,
                 mainTASK0_PRIORITY, NULL );
        xTaskCreate( task1, (signed char *) "TASK1",
                 configMINIMAL_STACK_SIZE, NULL,
                 mainTASK1_PRIORITY, NULL );
				xTaskCreate( console, (signed char *) "CONSOLE",
								configMINIMAL_STACK_SIZE, NULL,
                 mainTASK_CONSOLE_PRIORITY, NULL );
        /* Start the tasks running. */
        vTaskStartScheduler();
    }

    /* If all is well we will never reach here as the scheduler will now be
    running.  If we do reach here then it is likely that there was insufficient
    heap available for the idle task to be created. */
    for( ;; )
        ;
}
/*-----------------------------------------------------------*/

static void task0( void *pvParameters )
{
    portTickType xNextWakeTime = xTaskGetTickCount();
    
    while (1) {
////        * regGPIOB_BSRR = 1u<<11; // set bit 11
////        for (int i=0; i < 51200; ++i)
////        {} // work
////        * regGPIOB_BSRR = 1u<<27; // clr bit 11
        vTaskDelayUntil(&xNextWakeTime, 50);
    }

}
static void task1( void *pvParameters )
{
    portTickType xNextWakeTime = xTaskGetTickCount();
    
    while (1) {
//        * regGPIOB_BSRR = 1u<<12; // set bit 12
//        for (int i=0; i < 60000; ++i)
//        {}
//        * regGPIOB_BSRR = 1u<<28; // clr bit 12
        vTaskDelayUntil(&xNextWakeTime, 200);
    }

}

static void console (void *pvParameters )
{
	int8_t returnedValue = ' ';
	char stringBuff[15] = {' '};
	uint8_t stringBuffLocation = 0;
	char command = '0';
	int32_t resultFromCommand = 0;
	portTickType xNextWakeTime = xTaskGetTickCount();
	while (1)
	{
		returnedValue = USART2GetByte(' ');
		
		USART2SendByte(returnedValue);
		
		stringBuff[stringBuffLocation] = returnedValue;
		stringBuffLocation++;
		
		//If we recieve a backspace, remove a char, re-write overtop of the old 
		//letter, and then go back a space again.  Don't go back more than 1, if 
		//They hit backspace at the start of the line.
		//We also take care of strings that are too long with this same command.
		if(returnedValue == '\b' || returnedValue == 127 || stringBuffLocation > 13) 
		{
			UIRemoveBackspace(stringBuffLocation);
		}
		if(returnedValue == '\r')
		{
				
			if(stringBuffLocation > 10)
			{
				UIMessageTooLong();
				command = 'H';
			}
			else
			{
				//USART2SendString("In MatchString\n");
				command = UImatchString(stringBuff, stringBuffLocation);
			}
			stringBuffLocation = 0;
			stringBuff[0] = ' ';
			stringBuff[1] = ' ';
			stringBuff[2] = ' ';
			stringBuff[3] = ' ';
			stringBuff[4] = ' ';
			stringBuff[5] = ' ';
			stringBuff[6] = ' ';
			stringBuff[7] = ' ';
			stringBuff[8] = ' ';
			stringBuff[9] = ' ';
			stringBuff[10] = ' ';
			stringBuff[11] = ' ';
			stringBuff[12] = ' ';
			stringBuff[13] = ' ';
			stringBuff[14] = ' ';
			//USART2SendNumber32(stuff);
			resultFromCommand = UILEDCommand(command);
			UIConsole();
		}
		if (resultFromCommand != 0)
		{
			GLOBAL_BALLHEIGHT = resultFromCommand;	
		}
		
		vTaskDelayUntil(&xNextWakeTime, 50);
	}
}

static void prvQueueSendTask( void *pvParameters )
{
portTickType xNextWakeTime;
const unsigned long ulValueToSend = 100UL;

    /* Initialise xNextWakeTime - this only needs to be done once. */
    xNextWakeTime = xTaskGetTickCount();

    for( ;; )
    {
        /* Place this task in the blocked state until it is time to run again.
        The block time is specified in ticks, the constant used converts ticks
        to ms.  While in the Blocked state this task will not consume any CPU
        time. */
        vTaskDelayUntil( &xNextWakeTime, mainQUEUE_SEND_FREQUENCY_MS );

        /* Send to the queue - causing the queue receive task to unblock and
        print out a message.  0 is used as the block time so the sending 
        operation will not block - it shouldn't need to block as the queue 
        should always be empty at this point in the code. */
        xQueueSend( xQueue, &ulValueToSend, 0 );
    }
}
/*-----------------------------------------------------------*/

static void prvQueueReceiveTask( void *pvParameters )
{
unsigned long ulReceivedValue;

    for( ;; )
    {
        /* Wait until something arrives in the queue - this task will block
        indefinitely provided INCLUDE_vTaskSuspend is set to 1 in
        FreeRTOSConfig.h. */
        xQueueReceive( xQueue, &ulReceivedValue, portMAX_DELAY );

        /*  To get here something must have been received from the queue, but
        is it the expected value?  If it is, print out a pass message, if no,
        print out a fail message. */
        if( ulReceivedValue == 100UL )
        {
            printf( "Value 100 received - Tick = 0x%x\r\n", xTaskGetTickCount() );
        }
        else
        {
            printf( "Received an unexpected value\r\n" );
        }
    }
}
/*-----------------------------------------------------------*/
extern "C" 
{
	int fputc( int iChar, FILE *pxNotUsed ) 
	{
			/* Just to avoid compiler warnings. */
			( void ) pxNotUsed;

			if( mainDEMCR & mainTRCENA ) 
			{
					while( mainITM_Port32( 0 ) == 0 );
					mainITM_Port8( 0 ) = iChar;
			}

			return( iChar );
	}
}
