#include "stm32f10x.h"

void IRSensorInit(void);
void FindPositionSensor(void);
int FindPosition(void);  //Returns height in CM
void InitializeHeight(void);
int LinearInterpolation (int x1, int y1, int x3, int y3, int x2);

const int TubeLength = 61;
