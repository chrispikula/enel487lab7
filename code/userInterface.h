/**
Programmer: Chris Pikula
Project: Ball Control
Date: 2016-11-15

Description: This is part of a program that turns on and off
LEDs via communicating to and from the console.
It can also run some timing tests, as well as use an interrupt timer
	(Via timers 2 and 3, respectively)

It can also setup the ball-control system.

This file is header file for the user interface
*/

#include "portXInit.h"
//Used to for portBInit
#include "serialPortInit.h"
//Used for USART2Init
#include "utilities.h"
//For variable initialization
#include "IRSensor.h"
//For IR Sensor usage

bool CompareCharArrays(const char * typedInput, const int8_t length, const char * comparedInput);
char UImatchString(char* input, int8_t length);
uint16_t UILEDCommand(char command);
char UIRemoveBackspace(uint8_t&);
void UIMessageTooLong(void);
void UIConsole(void);
uint32_t UIInputNumber(uint32_t min, uint32_t max);
uint32_t AtoI(char* input);

