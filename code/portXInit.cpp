/**
Programmer: Chris Pikula
Project: ServoMotor
Date: 2016-10-24

Description: This is part of a program that turns on and off
LEDs via communicating to and from the console.
It can also run some timing tests, as well as use an interrupt timer
	(Via timers 2 and 3, respectively)

Description: This is a simple file that just, currently, initializes port B
*/

#include "portXInit.h"

void portBInit(void)
{
	RCC->APB2ENR |= 0x08;
	//RCC->APB2ENR |= 0x08;
	GPIOB->ODR &= 0x0000FF00;
	//GPIOB->ODR &= 0x0000FF00;
	GPIOB->CRH = 0x33333333;//BB;
	 // configure PB9 as TIM4_CH4 output
  //      (0xBu<<4) | // bits 7:4,  PB9,  output mode, 10MHz, alt func open drain
        // configure PB8 as TIM4_CH3 output
   //     (0xBu<<0);  // bits 3:0,  PB8,  output mode, 10MHz, alt func open drain

	GPIOB->CRL &= 0x0FFFFFFF;
	GPIOB->CRL |= 0xB0000000; //need PB7 for TIMER4
	//GPIOB->CRH = 0x33333333;
	/**
	It doesn't make sense to me to set all of the pins to state 3, but I haven't
	done enough research yet to make sure that *not* setting them to 50MHz is a 
	good idea
	*/
}
